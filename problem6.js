const problem6 = (inv) => {
    let AudiAndBMW = []
    for (let i=0; i<inv.length; i++) {
        if (inv[i]["car_make"].toUpperCase() === 'AUDI' || inv[i]["car_make"].toUpperCase() === 'BMW') {
            AudiAndBMW.push(inv[i]);
        }
    }
    return AudiAndBMW;
}


// export { problem6 };
module.exports.problem6 = problem6;