const problem2 = (inv) => {
    return `Last car is ${inv[inv.length-1]["car_make"]} ${inv[inv.length-1]["car_model"]}`;
}

// export { problem2 };

module.exports.problem2 = problem2;