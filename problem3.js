const problem3 = (inv) => {
    for (let i=0; i<inv.length; i++) {
        for (let j=i; j<inv.length; j++) {
            if ( inv[j]["car_model"].toUpperCase() < inv[i]["car_model"].toUpperCase()) {
                let temp = inv[j];
                inv[j] = inv[i];
                inv[i] = temp;
            }
        }
    }
    return inv
}

// export { problem3 };
module.exports.problem3 = problem3;