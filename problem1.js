const problem1 = (inv) => {
    for (let i=0; i<inv.length; i++) {
        if (inv[i]["id"] === 33) {
            return `Car 33 is ${inv[i]["car_year"]} ${inv[i]["car_make"]} ${inv[i]["car_model"]}`;
        }
    }
}

module.exports.problem1 = problem1;