// import { problem4 } from './problem4.js';
var temp = require('./problem4.js');

const problem5 = (inv) => {
    let olderCars = []
    let years = temp.problem4(inv);
    for (let i=0; i<years.length; i++) {
        if (years[i] < 2000) {
            olderCars.push(years[i]);
        }
        // if (inv[i]["car_year"] < 2000) {
        //     olderCars.push(inv[i]["car_year"]);
        // }
    }
    return olderCars.length;
}

// export { problem5 };
module.exports.problem5 = problem5;